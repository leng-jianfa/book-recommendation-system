package com.controller;

import com.model.Book;
import com.service.BookService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;
    private String username;
    private String type;
    private String result;

    @RequestMapping("/delete")
    public String delete(Book book, ServletRequest request) {
        bookService.delete(book.getId());
        return "redirect:/selectBook";
    }

    @RequestMapping("/selectBook")
    public String selectBook(ServletRequest request) throws IOException {
        //显示书籍数据
        List<Book> list = bookService.selectBook();
        request.setAttribute("bookList", list);
    //
//        String username=request.getParameter("username");
//        System.out.println("username-------------"+username);
//        //通过名字查询userId
//        int userid= userService.selectIdByUsername(username);
//        System.out.println("userId-------------"+userid);
//        //加入flask产生的网络地址
//        InputStream recommend = new URL("http://127.0.0.1:5000/recommend"+userid).openStream();
//        byte[] bytes = new byte[0];
//        bytes=new byte[recommend.available()];
//        recommend.read(bytes);
//        String str = new String(bytes);
//        String str1= StringUtils.replace(str,"[","");
//        String str2= StringUtils.replace(str,"]","");
//        System.out.println("返回的编号："+str2);
//        //String 转化为List
//        String[] lis = str2.split(",");
//        System.out.println("编号集合："+lis);
//        List<Book> recommendList = new ArrayList<Book>();
//        //查询书籍信息
//        for(String i:lis){
//            System.out.println("-------:"+i);
//            Book book=bookService.selectBookList(i);
//            System.out.println("查询的书籍信息："+book);
//            recommendList.add(book);
//        }
//
//
//        model.addObject("userList",result);
//        model.setViewName("userList");  //将查的结果保存到userList
//        System.out.println("所有结果："+result);

        return "index";
    }

    @RequestMapping("/selectBook1")
    public String selectBook1(String type,ServletRequest request) {
        //显示书籍数据
        List<Book> list = bookService.selectBook1(type);
        request.setAttribute("book1List", list);
        return "classification";
    }

    //搜索
    @RequestMapping("/search")
    public String search(String search,ServletRequest request){
        List<Book> list = bookService.search(search);
        System.out.println("search:"+search);
        request.setAttribute("searchList", list);
        System.out.println(list);
        return "search";
    }

    //详情
    @RequestMapping("/particulars")
    public String particulars(String name, Book book, ServletRequest request) {
        //显示书籍详情
        List<Book> list = bookService.selectparticulars(name);
        System.out.println(list);
        request.setAttribute("bookList", list);
        return "particulars";
    }

    //详情
//详情
    @RequestMapping("/particulars1")
    public String particulars1(String name, Book book, ServletRequest request) {
        //显示书籍详情
        List<Book> list = bookService.selectparticulars(name);
        System.out.println(list);
        request.setAttribute("bookList", list);
//        request.setAttribute("Label",book.getLabel().replaceAll("\\d", ""));
        return "particulars1";
    }

    @RequestMapping("/update")
    public String updetebook(Book book, ServletRequest request) {
        request.setAttribute("id", book.getId());
        request.setAttribute("pictureUrl", book.getPictureUrl());
        request.setAttribute("name", book.getName());
        request.setAttribute("author", book.getAuthor());
        return "update";
    }

    @RequestMapping("goUpdate")
    public String goUpdate(Book book) {
        bookService.update(book);
        return "redirect:/selectBook";
    }
}
