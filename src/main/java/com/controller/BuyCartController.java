package com.controller;

import com.model.BuyCart;
import com.model.Book;
import com.service.BuyCartService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class BuyCartController {
    @Autowired
    private BuyCartService buyCartService;
    @Autowired
    private UserService userService;
    private String username;

    @RequestMapping("/goBuyCart")
    public String goBuyCart(HttpServletRequest request) {
        String username=request.getParameter("username");
        System.out.println("username-------------"+username);
        List<BuyCart> list = buyCartService.selectBuyCart(username);  //去查询购物车
        request.setAttribute("buyCartList", list);  //封装成buyCart
        System.out.println(list);
        return "buycart";
    }

    @RequestMapping("/addCart")
    public String addCart(Book book,HttpServletRequest request) {

        String pictureUrl = book.getPictureUrl();
        String name = book.getName();
        double price = book.getPrice();
        String username=request.getParameter("username");
        System.out.println("username-------------"+username);
        //通过名字查询userId
        int userid= userService.selectIdByUsername(username);
        System.out.println("userId-------------"+userid);
        buyCartService.addCart(book,userid);
        return "redirect:/selectBook";
    }

    @RequestMapping("deleteCart")
    public String deleteCart(int id){
        buyCartService.deleteCart(id);
        return "redirect:/selectBook";
    }
}

