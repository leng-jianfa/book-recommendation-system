package com.controller;

import com.model.BuyCart;
import com.model.Book;
import com.model.Collect;
import com.service.CollectService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CollectController {
    @Autowired
    private CollectService collectService;
    @Autowired
    private UserService userService;
    private String username;

    @RequestMapping("/goCollect")
    public String goCollect(HttpServletRequest request) {
        String username=request.getParameter("username");
        System.out.println("username-------------"+username);
        List<Collect> list = collectService.selectCollect(username);  //去查询收藏
        request.setAttribute("collectList", list);  //封装成collect
        System.out.println(list);
        return "collect";
    }

    @RequestMapping("/addCollect")
    public String addCollect(Book book,HttpServletRequest request) {

        String pictureUrl = book.getPictureUrl();
        String name = book.getName();
        String author = book.getAuthor();
        String score = book.getScore();
        String type = book.getType();
        String label = book.getLabel();
        String baiduyun = book.getBaiduyun();
        String lanzyun = book.getLanzyun();
        String username=request.getParameter("username");
        System.out.println("username-------------"+username);
        //通过名字查询userId
        int userid= userService.selectIdByUsername(username);
        System.out.println("userId-------------"+userid);
        collectService.addCollect(book,userid);
        return "redirect:/selectBook";
    }
    @RequestMapping("deleteCollect")
    public String deleteCollect(String name){
        collectService.deleteCollect(name);
        return "redirect:/selectBook";
    }
}
