package com.controller;

import com.model.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    private String username;
    //    登录
    @RequestMapping("/login")
    public static String login(HttpServletRequest request, HttpServletResponse response) {
        return "login";
    }


    //    首页
    @RequestMapping("/index1")
    public String index1(User user, HttpServletResponse response, HttpServletRequest request) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        User result = userService.login(user);
        if (result != null) {
            HttpSession httpSession = request.getSession();
            httpSession.setAttribute("username", user.getUsername());
            httpSession.setAttribute("password", result.getPassword());
            httpSession.setAttribute("id", result.getId());
            System.out.println(result.getId());
            httpSession.setAttribute("pictureUrl", result.getPictureUrl());
            String username=request.getParameter("username");
//            List<User> result = userService.getUserList();
//            session.setAttribut("username",username);
//            if(session.getAttribute("username")!=null){
//
//            }
            return "index1";
        } else {
            response.getWriter().append("用户名或密码错误");
        }
        return null;
    }

    //注销登录
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request)
    {
        HttpSession httpSession = request.getSession();
        System.out.println("logout");
        //ssion失效
        httpSession.removeAttribute("username");
        httpSession.removeAttribute("id");
        httpSession.removeAttribute("password");
        httpSession.removeAttribute("pictureUrl");
        return "redirect:/login";
    }


//    商品页
    @RequestMapping("/index")
    public String index(){
        return "redirect:/selectBook";
    }

    //注册
    @RequestMapping("register")
    public String register() {
        return "register";
    }
    @RequestMapping("goRegister")
    public String goRegister(MultipartFile pictureFile,User user) {
        try {
            //处理二进制文件，保存到一个独立的文件夹
            String path="D:/images/users";
            String fileName=pictureFile.getOriginalFilename();
            System.out.println(path);
            System.out.println(fileName);
            File file=new File(path,fileName);
            pictureFile.transferTo(file);
            //处理文字数据
            String filePath="upload/users/";
            user.setPictureUrl(filePath+fileName);
            userService.register(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "login";
    }


    //找回密码
    @RequestMapping("gopsd")
    public String psd() {
        return "psd";
    }


    //    收藏
    @RequestMapping("collect")
    public String collect() {
        return "collect";
    }

    //    个人中心
    @RequestMapping("personal")
    public String personal(){
        return "personal";
    }
    @RequestMapping("gopersonal")
    public String gopersonal(MultipartFile pictureFile,User user,HttpServletRequest request) {
        try {
            //处理二进制文件，保存到一个独立的文件夹
            String path="D:/images/users";
            String fileName=pictureFile.getOriginalFilename();
            System.out.println(path);
            System.out.println(fileName);
            File file=new File(path,fileName);
            pictureFile.transferTo(file);
            //处理文字数据
            String filePath="upload/users/";
            user.setPictureUrl(filePath+fileName);
            userService.register1(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "index";
    }
    //地图
    @RequestMapping("map")
    public String map(){
        return "map";
    }
}
