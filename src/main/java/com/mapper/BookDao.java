package com.mapper;

import com.model.Book;

import java.util.List;

public interface BookDao {
    List<Book> selectBook();

    List<Book> selectBook1(String type);

    void delete(int id);

    List<Book> selectparticulars(String name);
    List<Book> search(String search);
    void update(int id);

}
