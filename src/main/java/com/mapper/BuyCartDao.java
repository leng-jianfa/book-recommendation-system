package com.mapper;

import com.model.Book;
import com.model.BuyCart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BuyCartDao {
    List<BuyCart> selectBuyCart(String username);
//    类型不同时，用@Param
    void addCart(@Param("book") Book book,@Param("userid") int userid);
    void deleteCart(int id);

}

