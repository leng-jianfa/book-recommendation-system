package com.mapper;

import com.model.Book;
import com.model.BuyCart;
import com.model.Collect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectDao {
    List<Collect> selectCollect(String username);
    //    类型不同时，用@Param
    void addCollect(@Param("book") Book book, @Param("userid") int userid);
    void deleteCollect(String name);
}
