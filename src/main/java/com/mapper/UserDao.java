package com.mapper;

import com.model.User;

public interface UserDao {
    User login(User user);
    void register(User user);
    int selectIdByUsername(String username);
    void register1(User user);
}