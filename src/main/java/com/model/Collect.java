package com.model;

public class Collect {
    private int id;
    private String name;  //书名
    private String author;  //作者
    private Double score;  //豆瓣评分
    private String type;  //类型
    private String label;  //标签
    private String publishedmonthly;  //出版年月
    private String baiduyun;  //百度云链接
    private String lanzyun;  //蓝奏云链接
    private String format;  //格式
    private int userid;
    private String pictureUrl;//图片

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPublishedmonthly() {
        return publishedmonthly;
    }

    public void setPublishedmonthly(String publishedmonthly) {
        this.publishedmonthly = publishedmonthly;
    }

    public String getBaiduyun() {
        return baiduyun;
    }

    public void setBaiduyun(String baiduyun) {
        this.baiduyun = baiduyun;
    }

    public String getLanzyun() {
        return lanzyun;
    }

    public void setLanzyun(String lanzyun) {
        this.lanzyun = lanzyun;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
