package com.service;

import com.model.Book;

import java.util.List;

public interface BookService {
    List<Book> selectBook();
    List<Book> selectBook1(String type);
    void delete(int id);

    void update(Book book);
    List<Book> selectparticulars(String name);

    List<Book> search(String search);

}
