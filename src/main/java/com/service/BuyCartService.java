package com.service;


import com.model.BuyCart;
import com.model.Book;

import java.util.List;

public interface BuyCartService {
    List<BuyCart> selectBuyCart(String username);
    void addCart(Book book,int userid);
    void deleteCart(int id);
}

