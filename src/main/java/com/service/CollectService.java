package com.service;

import com.model.Book;
import com.model.BuyCart;
import com.model.Collect;

import java.util.List;

public interface CollectService {
    List<Collect> selectCollect(String username);
    void addCollect(Book book, int userid);
    void deleteCollect(String name);
}
