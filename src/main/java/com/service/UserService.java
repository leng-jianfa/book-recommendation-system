package com.service;

import com.model.User;

public interface UserService {
    User login(User user);

    void register(User user);
    void register1(User user);

    int selectIdByUsername(String username);
}