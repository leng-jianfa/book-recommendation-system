package com.servicelmpl;

import com.mapper.BookDao;
import com.model.Book;
import com.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServicelmpl implements BookService {
    @Autowired
    private BookDao bookDao;

    @Override
    public List<Book> selectBook() {
        List<Book> list = bookDao.selectBook();
        return list;
    }
    @Override
    public List<Book> selectBook1(String type) {
        List<Book> list = bookDao.selectBook1(type);
        return list;
    }


    @Override
    public void delete(int id) {
        bookDao.delete(id);
    }

    @Override
    public void update(Book book) {

    }
    @Override
    public List<Book> selectparticulars(String name) {
       return bookDao.selectparticulars(name);
    }
    @Override
    public List<Book> search(String search) {
        return bookDao.search(search);
    }
}
