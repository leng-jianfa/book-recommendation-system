package com.servicelmpl;

import com.mapper.BuyCartDao;
import com.model.BuyCart;
import com.model.Book;
import com.service.BuyCartService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuyCartServicelmpl implements BuyCartService {
    @Autowired
    BuyCartDao buyCartDao;
    @Override
    public List<BuyCart> selectBuyCart(String username) {
        List<BuyCart> list = buyCartDao.selectBuyCart(username);
        return list;
    }
    @Override
    public void addCart(Book book,int userid) {
        buyCartDao.addCart(book, userid);
    }

    @Override
    public void deleteCart(int id){
        buyCartDao.deleteCart(id);
    }
}