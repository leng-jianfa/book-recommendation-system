package com.servicelmpl;

import com.mapper.CollectDao;
import com.model.Book;
import com.model.Collect;
import com.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectServicelmpl implements CollectService {
    @Autowired
    CollectDao collectDao;
    @Override
    public List<Collect> selectCollect(String username) {
        List<Collect> list = collectDao.selectCollect(username);
        return list;
    }
    @Override
    public void addCollect(Book book, int userid) {
        collectDao.addCollect(book, userid);
    }
    @Override
    public void deleteCollect(String name){
        collectDao.deleteCollect(name);
    }
}
