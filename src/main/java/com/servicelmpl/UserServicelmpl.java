package com.servicelmpl;

import com.mapper.UserDao;
import com.model.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServicelmpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public User login(User user) {
        User result = userDao.login(user);
        return result;
    }

    @Override
    public void register(User user) {
        userDao.register(user);
    }
    @Override
    public void register1(User user) {
        System.out.println("-----id:"+user.getId());
        System.out.println("-----username:"+user.getUsername());
        System.out.println("-----password:"+user.getPassword());
        System.out.println("-----pictureUrl:"+user.getPictureUrl());
        System.out.println("-----sex:"+user.getSex());
        System.out.println("-----birth:"+user.getBirth());
        System.out.println("-----describe:"+user.getDescribe());
        userDao.register1(user);
    }

    @Override
    public int selectIdByUsername(String username){
        int userid=userDao.selectIdByUsername(username);
        return userid;
    }
}
