<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/4/17
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%--数据页--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="zh-CN">
<!--轮播图的js-->

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../../css/style11.css" rel="stylesheet">
    <script src="../../js/jquery.min.js" type="text/javascript"></script>
    <title>首页</title>
</head>
<body>

<!--头部模块-->
<div class="header">
    <div class="logo1">
        <p style="font-size:40px">轻氢书店</p>
    </div>

    <div class="nav">
        <ul>
            <li><a href="#">首页</a></li>
            <li ><a href="#">排行榜</a></li>
            <li><a href="/goCollect?username=${username}">收藏</a></li>
            <li><a href="/map">个人定位</a></li>
        </ul>
    </div>

    <div class="search">
        <label>
            <input type="text" placeholder="请输入" id="cz">
        </label>
        <button style="background-color: #ff7c2d" id="ss">搜索</button>
    </div>


    <div class="titleBar">
        <span>
            <img class="txiang" src="${pictureUrl}" alt="">
        </span>
        <div class="title1">
            <ul>
                <li><a href="/goBuyCart?username=${username}" class="gwc">购物车</a></li>
                <li><a href="/personal?id=${id}" class="grzx">个人中心</a></li>
                <li><a href="/logout" class="tcdl">退出登录</a></li>
            </ul>
        </div>
    </div>
</div>

<!--轮播图-->
<div class="imgBox">
    <img class="img-slide img1" src="https://img-blog.csdnimg.cn/89f64074164e45c3bd18629c72e35419.jpeg" alt="1">
    <img class="img-slide img2" src="https://img-blog.csdnimg.cn/9b335ee9ca0440e191b752eaf67ee031.jpeg" alt="2">
    <img class="img-slide img3" src="https://img-blog.csdnimg.cn/ea3c1c3c297a4610bd22a25abc442f45.jpeg" alt="3">
</div>

<!--推荐的导航栏部分-->
<div class="goods w">
    <h3>推荐</h3>
    <div class="goods-item">
        <%--   books表里面的type字段     --%>
        |<a href="/selectBook1?type=励志成功">小说文学</a>
        |<a href="/selectBook1?type=学习教育">学习教育</a>
        |<a href="/selectBook1?type=经济管理">经济管理</a>
        |<a href="/selectBook1?type=编程开发">编程开发</a>
    </div>
<%--    <div class="mod">--%>
<%--        修改兴趣--%>
<%--    </div>--%>
</div>

<!--内容部分-->
<div class="goodsBox w">
    <div class="goodsBox-hd">
        <h3>精品推荐</h3>
        <a href="#">查看全部</a>
    </div>

    <div>
        <a href="#">
            <div class="goodsBox-bd clearfix">
                <c:forEach items="${bookList}" var="li" varStatus="status">
                    <a href="/particulars?name=${li.name}">
                        <ul>
                            <li>
                                    <img alt="" src="${li.pictureUrl}">
                                    <h4>${li.name}</h4>
                                    <h4>${li.author}</h4>
                            </li>
                        </ul>
                    </a>

                </c:forEach>
            </div>
        </a>
    </div>


</div>
</body>
<style type="text/css">
    .titleBar{
        margin-left: 10px;
        width: 100px;
        display: inline-block;
        position: relative;
    }
    .title1{
        width: 80px;
        height: 70px;
        border: 1px solid lightgray;
        /*background-color: #aeaeae;*/
        display: none;
        position: absolute;
    }
    .logo1{
        float: left;
        /*margin-top: 20px;*/
        margin-left: 10%;
    }
    .gwc {
        color: black;
    }
    .grzx{
        color: black;
    }
    .tcdl {
        color: black;
    }
    a:hover{
        color: blue;
    }
    .titleBar:hover .title1{display: block;}
</style>
<script type="text/javascript">
    //搜索传值
    $(function () {
        $("#ss").click(function () {
            var json = {
                search: $("#cz").val(),
            }
            $.post(
                "/search",

                json,
                function (data, text, XMLHTTPRequest) {
                    console.log(data)
                    console.log(text)
                    console.log(XMLHTTPRequest)
                    $("body").html(data)
                })
        })
    })
    var index = 0;

    //效果
    function ChangeImg() {
        index++;
        var a = document.getElementsByClassName("img-slide");
        if (index >= a.length) index = 0;
        for (var i = 0; i < a.length; i++) {
            a[i].style.display = 'none';
        }
        a[index].style.display = 'block';
    }
    //设置定时器，每隔两秒切换一张图片
    setInterval(ChangeImg, 3000);
</script>
</html>