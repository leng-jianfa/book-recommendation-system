<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/4/17
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%--首页--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <title>首页</title>

  <link rel="stylesheet" href="../../css/reset1.css">
  <link rel="stylesheet" href="../../css/index1.css">
</head>
<body>

  <div class="wrapper" id="wrapper">

    <div class="bg-white"></div>


<%--头部--%>
    <div class="header">
      <div class="container">
        <p class="logo" style="font-size:40px">轻氢书店</p>
        <ul class="nav">
          <li class="nav-item">
            <a href="/index">商城首页</a>
          </li>
          <li class="nav-item">
            <a href="#">排行榜</a>
          </li>
          <li class="nav-item">
            <a href="/collect">收藏</a>
          </li>
          <li class="nav-item"><a href="/map">个人定位</a></li>
        </ul>
<%--        用户头像--%>
        <div class="user">
          <div class="user-hover"></div>
          <div class="user-img">
            <img class="txiang" src="${pictureUrl}" alt="#">
            <span class="arrow-bg"></span>
            <span class="arrow-font"></span>
          </div>
          <div class="user-info">
<%--            头像下的选择--%>
            <ul>
              <li><a href="/goBuyCart?username=${username}">购物车</a></li>
              <li><a href="/personal?id=${id}">个人中心</a></li>
              <li><a href="/logout">退出登录</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="sub-nav">


  </div>

  <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
  <script src="../../js/sliders.js"></script>
  <script src="../../js/navChange.js"></script>
  <script>
    $(function () {

      var slider1 = new Slider1();
      var doc_h = $(document.body).width();

      slider1.inital('sliderBox', {
        slider_num: 4,
        w: doc_h,
        h: 760
      });
    });

  </script>

</body>
</html>