<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/4/17
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%--首页--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="zh-CN">
<head>
    <title>首页</title>
    <link rel="stylesheet" href="../../css/reset1.css">
    <link rel="stylesheet" href="../../css/index1.css">
</head>
<style type="text/css">

    .div {
        display: flex;
        justify-content: space-between;
    }

    .div2 {
        display: flex;
        align-items: center; /* 垂直居中 */
        margin-right: 10px;
        margin-bottom: 10px;
        flex-direction: column;
        justify-content: space-between;
    }

    * {
        margin: 0;
        padding: 0;
    }

    #container {
        width: 90%;
        background: #ccc;
        height: auto;
        margin: 0 auto;

    }



    #main1 {
        width: 60%;
        margin-top: 65px;
        margin-left: 10px;
        border-radius: 10px;
        background: white;
        padding-top: 10px;
    }

    .button1{
        width: 80px;
        height: 30px;
        background-color: #20bdfe;
        border-radius: 20px;
    }
    .button2{
        width: 50px;
        height: 30px;
        background-color: #20bdfe;
        border-radius: 20px;
    }

    .nr {
        display: flex;
    }

    .phb {
        width: 25%;
        height: 50%;
        background: white;
        border-radius: 10px;
        position: fixed;
        right: 10%;
        top: 30%;
        margin-top: -100px;
    }

    .phb-1 {
        text-align: center;
        font-size: 40px;
    }

</style>
<body>
<div id="container">
    <!--         头部-->
    <div class="header">
        <div class="container">
            <h1 class="logo"><a href="#"></a></h1>
            <ul class="nav">
                <li class="nav-item">
                    <a href="/index">商城首页</a>
                </li>
                <li class="nav-item" data-subNav="nav_iphone">
                    <a href="#">排行榜</a>
                </li>
                <li class="nav-item" data-subNav="nav_subIphone">
                    <a href="/collect">收藏</a>
                </li>
                <li class="nav-item" data-subNav="nav_voice">
                    <a href="/history">历史记录</a>
                </li>

            </ul>
            <%--        用户头像--%>
            <div class="user">
                <div class="user-hover"></div>
                <div class="user-img">
                    <img class="txiang" src="${pictureUrl}" alt="#">
                    <span class="arrow-bg"></span>
                    <span class="arrow-font"></span>
                </div>
                <div class="user-info">
                    <%--            头像下的选择--%>
                    <ul>
                        <li><a href="/goBuyCart">我的购物车</a></li>
                        <li><a href="/personal">个人中心</a></li>
                        <li><a href="/order">我的订单</a></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--    内容-->
    <div class="nr">
        <div id="main1">
            <a href="">
                <c:forEach items="${movieList}" var="li" varStatus="status">
                    <div class="div">
                        <div class="div1">
                            <li>${li.movieId}</li>
                            <li>
                                <image src="${li.pictureUrl}" width="200px" height="100px"></image>
                            </li>
                            <li>${li.title}</li>
                            <li>${li.genres}</li>
                        </div>
                        <div class="div2">
                            <div>
                                <button class="button1" id="buyCart" onclick="addCart('${li.pictureUrl}', '${li.title}', '${li.price}')">加入购物车</button>
                            </div>
                            <div>
                                <button class="button2">购买</button>
                            </div>
                        </div>
                    </div>
                    <%--            <div id="div2">--%>
                    <%--                <a href="/update?movieId=${li.movieId}&pictureUrl=${li.pictureUrl}&title=${li.title}&genres=${li.genres}">编辑</a>--%>
                    <%--                <a href="/delete?movieId=${li.movieId}">删除</a>--%>
                    <%--            </div>--%>
                    <hr>
                    <br>
                </c:forEach>
            </a>

        </div>
        <%--排行榜--%>
        <div class="phb">
            <div class="phb-1">排行榜</div>
        </div>
    </div>
</div>
</body>
<script>
    $(function(){
        $("#but").click(function(){
            var json={
                search:$("#search").val(),
            }
            $.post(
                "/searchMovies",
                json,
                function(data,text,XMLHTTPRequest){
                    console.log(data)
                    console.log(text)
                    console.log (XMLHTTPRequest)
                    $("body").html(data)
                })
        })
    })
    function addCart(pictureUrl,title, price) {
        alert("添加成功")
        window.location.href="/addCart?pictureUrl="+pictureUrl+"&title="+title+"&price="+price
    }
</script>
</html>
