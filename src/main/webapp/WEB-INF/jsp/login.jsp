﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登录</title>
    <script src="https://www.jq22.com/jquery/jquery-1.10.2.js" type=" "></script>
    <style type="text/css">
        /* CSS Document */
        * {
            margin: 0;
            padding: 0
        }

        ul li {
            list-style: none
        }

        img {
            border: 0;
            max-width: 100%;
        }

        a {
            text-decoration: none;
            color: #333
        }

        .clear {
            clear: both
        }

        body {
            font-family: "微软雅黑";
            width: 100%;
            margin: 0;
            width: 100%;
            height: 100vh;
            color: #fff;
            background: linear-gradient(-45deg, #231f6e, #20bdfe, #2600b4, #1d1a44);
            background-size: 400% 400%;
            animation: gradientBG 15s ease infinite;
            overflow: hidden
        }

        @keyframes gradientBG {
            0% {
                background-position: 0% 50%;
            }
            50% {
                background-position: 100% 50%;
            }
            100% {
                background-position: 0% 50%;
            }
        }

        a:hover {
            text-decoration: underline
        }

        .loginBox {
            width: 500px;
            height: 480px;
            background: url(../../images/搞笑图片.jpg) no-repeat;
            background-size: 100% 100%;
            overflow: hidden;
            border-radius: 50px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .loginBox h1 {
            color: #fff;
            text-align: center;
            font-weight: bold;
            font-size: 26px;
            margin-top: 90px;
            letter-spacing: 7px;
            margin-bottom: 20px
        }

        .loginBox .item {
            width: 300px;
            height: 45px;
            margin-left: 105px;
            border: 1px solid #1d90c5;
            margin-bottom: 10px;
            background: rgba(11, 116, 180, 0.8);
            display: flex
        }

        .loginBox .item .icon {
            float: left;
            width: 45px;
        }

        .loginBox .item .icon img {
            display: block;
            width: 20px;
            margin: 12px;
        }

        .loginBox .item .txt {
            flex: 1;
        }

        .loginBox .item .txt input {
            border: 0;
            background: none;
            outline: none;
            width: 100%;
            height: 45px;
            line-height: 45px;
            color: #fff;
            letter-spacing: 1px;
            font-size: 14px
        }

        .loginBox .item .yzm {
            float: right;
            width: 100px;
            overflow: hidden;
            padding-left: 10px
        }

        .loginBox .item .yzm img {
            display: block;
            width: 100px;
            height: 45px;
        }

        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: #fff;
            opacity: 1;
        }

        ::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: #fff;
            opacity: 1;
        }

        input:-ms-input-placeholder {
            color: #fff;
            opacity: 1;
        }

        input::-webkit-input-placeholder {
            color: #fff;
            opacity: 1;
        }

        .loginBox .item_2 {
            width: 300px;
            margin-left: 105px;
            overflow: hidden;
            padding-bottom: 20px
        }

        .loginBox .item_2 a {
            float: right;
            color: #fff;
            font-size: 12px
        }

        .loginBox .item_2 input {
            float: left;
        }

        .loginBox .item_2 span {
            float: left;
            color: #fff;
            font-size: 12px
        }

        .loginBox .item_3 {
            width: 300px;
            margin-left: 105px;
            overflow: hidden
        }

        .loginBox .item_3 .btn {
            width: 300px;
            height: 45px;
            background: #3FB5E3;
            border: 0;
            outline: none;
            text-align: center;
            line-height: 45px;
            color: #fff;
            letter-spacing: 2px;
            font-size: 16px;
            font-weight: bold;
            border: 1px solid #1d90c5;
            margin-bottom: 10px;
            cursor: pointer
        }

        input[type=checkbox] {
            margin-right: 5px;
            cursor: pointer;
            font-size: 14px;
            width: 11px;
            height: 10px;
            position: relative;
        }

        input[type=checkbox]:after {
            position: absolute;
            width: 6px;
            height: 13px;
            top: 0;
            content: " ";
            background: rgba(11, 116, 180, 1);
            border: 1px solid #1d90c5;
            color: #fff;
            display: inline-block;
            visibility: visible;
            padding: 0px 3px;
            border-radius: 3px;
        }

        input[type=checkbox]:checked:after {
            content: "✓";
            font-size: 12px;
        }

        .loginBox .item_4 {
            width: 300px;
            margin-left: 105px;
            overflow: hidden;
            margin-top: 10px;
            color: #fff;
            font-size: 12px;
            letter-spacing: 0.5px
        }

        .loginBox .item_4 a {
            color: #1d90c5;
            font-size: 12px;
            font-weight: bold;
            padding-left: 3px
        }

        .loginBox .item_5 {
            width: 300px;
            margin-left: 105px;
            overflow: hidden;
            margin-top: 80px;
            letter-spacing: 0.5px;
            margin-bottom: 70px
        }

        .loginBox .item_5 p {
            text-align: center;
            color: #fff;
            font-size: 14px;
        }

    </style>
</head>

<body>
<form action="/index1">
    <div class="loginBox">
        <h1>欢迎登陆</h1>
        <div class="item">
            <div class="icon"><img src="../../images/icon1.png" alt=""/></div>
            <div class="txt"><label>
                <input name="username" type="text" placeholder="请输入您的用户名"/>
            </label></div>
        </div>
        <div class="item">
            <div class="icon"><img src="../../images/icon2.png" alt=""/></div>
            <div class="txt"><label>
                <input name="password" type="password" placeholder="请输入您的密码"/>
            </label></div>
        </div>
        <div class="item_2">
            <label>
                <input name="" type="checkbox" value=""/>
            </label>
            <span>记住密码</span>
            <a href="/gopsd">忘记密码？</a>
        </div>
        <div class="item_3">
            <input name="" type="submit" value="安全登录" class="btn"/>
        </div>
        <div class="item_4">
            还没有账号？<a href="/register">立即注册</a>
        </div>
    </div>
</form>
</body>
</html>
