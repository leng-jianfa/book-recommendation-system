<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/5/14
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>详情页面</title>
</head>
<style type="text/css">
    .tushu1 {
        width: 180px;
        height: 240px;
        margin-left: 30px;
        margin-right: 10px;
        float: left;
    }

    .tushu {
        width: 180px;
        height: 240px;
    }

    .xx {
        height: 240px;
    }

    .div4 {
        margin-top: 100px;
        height: 60px;
        width: 450px;
        margin-left: 60%;
    }

    .button4 {
        width: 100px;
        height: 50px;
        float: left;
        background-color: #ff7c2d;
        border: 0;
        border-radius: 10px;
        cursor: pointer;
    }

    /*鼠标移动到上面改变颜色*/
    button:hover {
        background-color: #ff4400;
    }

    .button6 {
        width: 100px;
        height: 50px;
        float: left;
        margin-left: 40px;
        background-color: #ff7c2d;
        border: 0;
        border-radius: 10px;
        cursor: pointer;
    }

    .button3 {
        width: 100px;
        height: 50px;
        margin-left: 40px;
        background-color: #ff7c2d;
        border: 0;
        border-radius: 10px;
        cursor: pointer;
    }
</style>
<body>
<c:forEach items="${bookList}" var="li" varStatus="status">
    <div class="tushu1">
        <img src="${li.pictureUrl}" class="tushu">
    </div>
    <div class="xx">
        <p>作者:${li.author}</p>
        <p>书名:${li.name}</p>
        <p>出版年月:${li.publishedmonthly}</p>
        <p>评分:${li.score}</p>
        <p>类型:${li.type}</p>
        <p onclick="clearString('${li.label}')">标签:${li.label}</p>
        <p>价格:${li.price}元</p>
    </div>

    <div class="div4">
        <div>
            <a href="/deleteCollect?name=${li.name}"><button class="button4" id="collect">取消</button></a>
        </div>
        <div>
            <button class="button6" id="buyCart"
                    onclick="addCart('${li.pictureUrl}', '${li.name}', '${li.price}','${username}')">加入购物车
            </button>
        </div>
        <div>
            <button class="button3" id="gm" onclick="addgm">购买</button>
        </div>
    </div>
</c:forEach>

</body>
<script>
    // //搜索传值
    // $(function () {
    //     $("#but").click(function () {
    //         var json = {
    //             search: $("#search").val(),
    //         }
    //         $.post(
    //             "/searchMovies",
    //
    //             json,
    //             function (data, text, XMLHTTPRequest) {
    //                 console.log(data)
    //                 console.log(text)
    //                 console.log(XMLHTTPRequest)
    //                 $("body").html(data)
    //             })
    //     })
    // })

    function addCart(pictureUrl, name, price, username) {
        alert("添加成功")
        window.location.href = "/addCart?pictureUrl=" + pictureUrl + "&name=" + name + "&price=" + price + "&username=" + username
    }


    // function addCollect(id) {
    //     alert("取消成功")
    //     window.location.href = "/addCollect?name=" + name + "&author=" + author + "&score=" + score + "&type=" + type + "&label=" + label + "&baiduyun=" + baiduyun + "&lanzyun=" + lanzyun + "&pictureUrl=" + pictureUrl + "&username=" + username
    // }
    function addgm(){
        alert("购买成功")
    }

    // function clearString(label){
    //     var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“'。，、？]")
    //     var rs = "";
    //     for (var i = 0; i < label.length; i++) {
    //         rs = rs+label.substr(i, 1).replace(pattern, '');
    //     }
    //     return rs;
    // }
</script>
</html>