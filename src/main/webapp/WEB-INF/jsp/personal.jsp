<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/5/14
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>个人主页</title>
</head>
<style type="text/css">
    .zhxx{
        text-align: center;
    }
    .zhxx1{
        margin-top: 10px;
        text-align: center;
    }
    .grtx{
        margin-left: 47%;
        margin-top: 2%;
    }
    .ygtx{
        width: 100px;
        height: 100px;
    }
    .f{
        height: 700px;
        background-image: url("../../images/grzxbj.png");
        background-size: 100% 100%;
    }
    .bd{
        margin-left: 40%;
    }
</style>
<body>
<div class="f">
    <div class="zhxx"><h1>个人信息中心</h1></div>
    <div class="grtx">
        <img src="${pictureUrl}" class="ygtx">
    </div>
    <div class="zhxx1"><h3>昵称：${username}</h3></div>

    <form id="register1" action="/gopersonal" method="post" enctype="multipart/form-data" class="bd">
        <!--        maxlength定义最大字符个数     placeholder定义框内提示信息-->
        <p>昵称（账号）：<input type="text" name="username" maxlength="11" placeholder="${username}"></p>
        <p>密码：<input type="password" name="password" maxlength="6" placeholder="${password}"></p>
        <p>编号：<input type="text" name="id" maxlength="6" placeholder="您的编号为${id}，请输入"></p>

        <!--        单选-->
        <p>
            <label>
                <input type="radio" name="sex" value="男">男
                <input type="radio" name="sex" value="女">女
            </label>
        </p>
        <!--        &lt;!&ndash;        下拉菜单&ndash;&gt;-->
        <!--        <p>-->
        <!--            籍贯：<select name="address">-->
        <!--            <option>重庆</option>-->
        <!--            <option>北京</option>-->
        <!--            <option>上海</option>-->
        <!--            <option>广东</option>-->
        <!--            <option>杭州</option>-->
        <!--            <option>浙江</option>-->
        <!--            <option>陕西</option>-->
        <!--            <option>云南</option>-->
        <!--            <option>江苏</option>-->
        <!--        </select>-->
        <!--        </p>-->

        <p>
            出生日期：<input type="date" name="birth">
        </p>

        <!--        上传文件-->
        <p>
            修改头像：<input type="file" name="pictureFile">
        </p>

        <!--        文本域，可换行  cols:长  rows:宽-->
        <p>
            个人描述：
        </p>
        <p>
            <textarea cols="50" rows="10" name="describe" type="text" placeholder="${describe}"></textarea>
        </p>

        <p>
            <input type="submit" value="提交按钮">
            <input type="reset" value="重置按钮">
        </p>


    </form>
</div>

</body>
</html>
