<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/5/14
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../../css/style11.css" rel="stylesheet">
    <title>排行榜</title>
</head>
<body>


<!--内容部分-->
<div class="goodsBox w">
    <div class="goodsBox-hd">
        <h3>全站前十排行榜</h3>
        <a href="#">查看全部</a>
    </div>

    <div>
        <a href="#">
            <div class="goodsBox-bd clearfix">
                <c:forEach items="${collectList}" var="li" varStatus="status">
                    <a href="/particulars1?name=${li.name}">
                        <ul>
                            <li>
                                <img alt="" src="${li.pictureUrl}">
                                <h4>${li.name}</h4>
                                <h4>${li.author}</h4>
                            </li>
                        </ul>
                    </a>

                </c:forEach>
            </div>
        </a>
    </div>


</div>
</body>
<style type="text/css">
    .titleBar{
        margin-left: 20px;
        width: 100px;
        display: inline-block;
        position: relative;
    }
    .title1{
        width: 100px;
        height: 80px;
        border: 1px solid lightgray;
        /*background-color: #aeaeae;*/
        display: none;
        position: absolute;
    }
    .titleBar:hover .title1{display: block;}
</style>

</html>
