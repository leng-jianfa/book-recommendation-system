<%--
  Created by IntelliJ IDEA.
  User: lengj
  Date: 2022/4/17
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%--数据页--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="zh-CN">
<!--轮播图的js-->
<script type="text/javascript">
    //搜索传值
    $(function () {
        $("#ss").click(function () {
            var json = {
                search: $("#cz").val(),
            }
            $.post(
                "/search",

                json,
                function (data, text, XMLHTTPRequest) {
                    console.log(data)
                    console.log(text)
                    console.log(XMLHTTPRequest)
                    $("body").html(data)
                })
        })
    })
</script>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../../css/style11.css" rel="stylesheet">
    <title>首页</title>
</head>
<body>

<!--头部模块-->
<div class="header">
    <div class="logo1">
        <p style="font-size:40px">轻氢书店</p>
    </div>

    <div class="nav">
        <ul>
            <li><a href="#">首页</a></li>
            <li ><a href="#">排行榜</a></li>
            <li><a href="/goCollect?username=${username}">收藏</a></li>
            <li><a href="/map">个人定位</a></li>
        </ul>
    </div>

    <div class="search">
        <label>
            <input type="text" placeholder="请输入" id="cz">
        </label>
        <button style="background-color: #ff7c2d" id="ss">搜索</button>
    </div>


    <div class="titleBar">
        <span>
            <img class="txiang" src="${pictureUrl}" alt="">
        </span>
        <div class="title1">
            <ul>
                <li><a href="/goBuyCart?username=${username}" class="gwc">购物车</a></li>
                <li><a href="/personal" class="grzx">个人中心</a></li>
                <li><a href="/logout" class="tcdl">退出登录</a></li>
            </ul>
        </div>
    </div>
</div>

<!--内容部分-->
<div class="goodsBox w">
    <div class="goodsBox-hd">
        <h3>搜索结果</h3>
    </div>

    <div>
        <a href="#">
            <div class="goodsBox-bd clearfix">
                <c:forEach items="${searchList}" var="li" varStatus="status">
                    <a href="/particulars?name=${li.name}">
                        <ul>
                            <li>
                                <img alt="" src="${li.pictureUrl}">
                                <h4>${li.name}</h4>
                                <h4>${li.author}</h4>
                            </li>
                        </ul>
                    </a>

                </c:forEach>
            </div>
        </a>
    </div>


</div>
</body>
<style type="text/css">
    .titleBar{
        margin-left: 10px;
        width: 100px;
        display: inline-block;
        position: relative;
    }
    .title1{
        width: 80px;
        height: 70px;
        border: 1px solid lightgray;
        /*background-color: #aeaeae;*/
        display: none;
        position: absolute;
    }
    .logo1{
        float: left;
        /*margin-top: 20px;*/
        margin-left: 10%;
    }
    .gwc {
        color: black;
    }
    .grzx{
        color: black;
    }
    .tcdl {
        color: black;
    }
    a:hover{
        color: blue;
    }
    .titleBar:hover .title1{display: block;}
</style>
</html>